import os, json, csv, datetime
import pandas as pd

from reportlab.pdfgen import canvas
from flask import jsonify
from openpyxl import Workbook, load_workbook

from models import ScoredPhrase
from db import db_session


def open_xls_file(directory, file):
    wb = load_workbook('{}/{}'.format(directory, file))

    sheets = {}

    for s in wb.worksheets:

        # Collect the column headings (i.e. en_us, pt_br, en)
        headings = []
        for row in s.iter_rows(min_row=1, max_row=1, values_only=True):
            for cell in row[1:]:
                headings.append(cell)

        # Collect the language keys (the base phrase being translated)
        keys = []
        for col in s.iter_cols(min_col=1, max_col=2, values_only=True):
            for cell in col[1:]:
                keys.append(cell)

        for i, row in enumerate(s.iter_rows(min_row=2, max_row=s.max_row, values_only=True)):
            for idx, val in enumerate(row[1:]):
                print(headings[i])
                sheet = {
                    'lang': headings[i],
                    'string': val
                }
                sheets[keys[i]] = sheet

    return sheets


def open_csv_file(file):
    df = pd.read_csv(file)


def save_to_json(data, filename='data.json'):
    """
    Sent data must be in the form of a Dict object
    :param data:
    :param filename:
    :return:
    """

    convert = json.dumps(data)

    with open(os.path.join('files/downloads/') + filename, 'w+') as json_file:
        json_file.write(convert)

    json_file.close()

    return os.path.join('files/downloads/') + filename


def save_to_csv(data,filename='data.csv'):
    """
    Saves the passed data into a CSV file
    :param data:
    :param filename:
    :return:
    """
    with open(os.path.join('files/downloads/') + filename, 'w+') as csv_file:
        w = csv.DictWriter(csv_file, data.keys())
        w.writeheader()
        w.writerow(data)

    csv_file.close()

    return os.path.join('files/downloads/') + filename


def save_to_pdf(data, filename='data.pdf'):
    """
    Saves the passed data into a PDF
    :param data:
    :param filename:
    :return:
    """
    c = canvas.Canvas(filename)
    c.drawString(100, 750, "Welcome to Reportlab!")
    c.save()

    return os.path.join('files/downloads/') + filename


def save_string_to_db(string, translation, datetime_added=datetime.datetime.now(), ):
    """
    Saves the passed string and translation into the database so it can be scored and checked against.
    :param string:
    :param translation:
    :param datetime_added:
    :return:
    """
    s = ScoredPhrase(string, translation)
    db_session.add(s)
    db_session.commit()


def compare_translation_elo_scores(translation1, translation2):
    pass
