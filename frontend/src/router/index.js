import Vue from 'vue';
import Router from 'vue-router';
import axios from 'axios';

const routerOptions = [
  { path: '/', component: 'Home' },
  { path: '/about', component: 'About' },
];

/* eslint-disable */
const routes = routerOptions.map((route) => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`),
  };
});
/* eslint-enable */

Vue.prototype.$http = axios;
Vue.use(Router);

export default new Router({
  routes,
  mode: 'history',
});
