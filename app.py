import os, json, datetime

from flask import Flask, render_template, make_response, request, abort, url_for, redirect, send_from_directory, jsonify
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
from random import *
from flask_sqlalchemy import SQLAlchemy

from handlers import open_xls_file, save_to_json, save_to_csv
from db import init_db

UPLOAD_FOLDER = os.path.join('uploads')
ALLOWED_EXTENSIONS = ['txt', 'xls', 'xlsx', 'json']
FILE_READERS = {
    'json': json.load,
    'xls': open_xls_file,
    'xlsx': open_xls_file,
    'txt': open
}

app = Flask(__name__,
            static_folder="./dist/static",
            template_folder="./dist"
            )
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# Pre-defined functions necessary for formatting some sorts of data or data filtering.
def transform(text_file_contents):
    return text_file_contents


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

'''
This section is for routing/views. Everything inside controls actions dictated by the user or programme
through using different routes. It is also where I will define different API routes for the frontend
to use.
'''


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")


@app.route('/api/translate', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def translate():
    if request.method == 'POST':
        request_file = request.files['file']
        if not request_file:
            return "No file"

        filename = secure_filename(request_file.filename)

        # os.path.join is used so that paths work in every operating system
        request_file.save(os.path.join("files/uploads", filename))
        file_ext = filename.rsplit('.', 1)[1].lower()

        file_content = ''

        # You should use os.path.join here too.
        if allowed_file(filename) and file_ext in FILE_READERS:
            file_content = FILE_READERS[file_ext](os.path.join('files/uploads'), filename)

        json_file = save_to_json(file_content)
        csv_file = save_to_csv(file_content)

        files = [
            {
                'ext': 'JSON',
                'file': json_file
             },
            {
                'ext': 'CSV',
                'file': csv_file
            }
        ]

        return jsonify(files, file_content)

    message = 'Looks like you\'re trying to translate a file without uploading one first'

    return message


if __name__ == '__main__':
    app.run('0.0.0.0', 5000, True)
    init_db()
